#!/usr/bin/python3
import cgi

pedido = cgi.FieldStorage()

assinar = pedido.getvalue('assinar')

print('Content-type: text/html; charset=utf-8\n\n')

if not assinar:
    print('Obrigado pelo seu cadastro!')
else:
    print('tente novamente!')

print('<br>')